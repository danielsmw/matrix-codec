module Paths_matrix_codec (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/danielsmw/stacklibs/matrix-codec/.stack-work/install/x86_64-osx/lts-3.7/7.10.2/bin"
libdir     = "/Users/danielsmw/stacklibs/matrix-codec/.stack-work/install/x86_64-osx/lts-3.7/7.10.2/lib/x86_64-osx-ghc-7.10.2/matrix-codec-0.1.0.0-6ds107J792Z76zqfw2NOuh"
datadir    = "/Users/danielsmw/stacklibs/matrix-codec/.stack-work/install/x86_64-osx/lts-3.7/7.10.2/share/x86_64-osx-ghc-7.10.2/matrix-codec-0.1.0.0"
libexecdir = "/Users/danielsmw/stacklibs/matrix-codec/.stack-work/install/x86_64-osx/lts-3.7/7.10.2/libexec"
sysconfdir = "/Users/danielsmw/stacklibs/matrix-codec/.stack-work/install/x86_64-osx/lts-3.7/7.10.2/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "matrix_codec_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "matrix_codec_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "matrix_codec_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "matrix_codec_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "matrix_codec_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
