module Main where

import Codec.Matrix
import Data.Array.IArray ((!))

main :: IO ()
main = do mtx <- readMatrixFile "/Users/danielsmw/0000317.mtx" (1000,2000)
          print $ fmap (! (0,0)) mtx
