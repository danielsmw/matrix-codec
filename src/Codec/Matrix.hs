{-# LANGUAGE BangPatterns #-}

module Codec.Matrix where

import           Data.Array.Unboxed
import qualified Data.Text          as T
import qualified Data.Text.IO       as IO
import qualified Data.Text.Read     as R

readMatrixFile :: FilePath 
               -> (Int,Int)
               -> IO (Either String (UArray (Int,Int) Double))
readMatrixFile fp (m,n) = read_text R.double fp 
                      >>= ( return 
                          . make_array range
                          )
  where range = ((0,0), (m-1,n-1))

-- This is implemented exactly as range is implemented for Ix (a,b)
-- in Data.Ix, except that the ordering of increase in (x,y) coords
-- is flipped. Here, x increments before y.
range_matrix :: ((Int,Int), (Int,Int)) -> [(Int,Int)]
range_matrix ((l1,l2),(u1,u2)) =
    [ (i1,i2) | i2 <- range (l2,u2)
              , i1 <- range (l1,u1) ]
{-# INLINE range_matrix #-}

-- Given a filepath and some Reader operation (see Data.Text.Read),
-- return either a parse error string or a list of elements.
read_text :: R.Reader e
          -> FilePath 
          -> IO (Either String [e])
read_text reader fp = IO.readFile fp
                  >>= ( return
                      . fmap (map fst)
                      . sequence 
                      . map reader 
                      . T.words )  
{-# INLINE read_text #-}

-- Given a range specification and a list of elements, form an 
-- IArray out of them. 
make_array :: IArray a e
           => ((Int,Int), (Int,Int))
           -> Either String [e]
           -> Either String (a (Int,Int) e)
make_array range xs = xs >>= (return . array range . zip ixs)
  where ixs = range_matrix range
{-# SPECIALIZE INLINE 
        make_array :: ((Int,Int), (Int,Int)) 
                   -> Either String [Double]
                   -> Either String (UArray (Int,Int) Double) #-}

